package pl.edu.pwsztar

import spock.lang.Specification

class UserIdSpec extends Specification {

    def "should check correct size"() {
        given:
            UserIdChecker userId = new UserId("44051401458")
        when:
            def result = userId.correctSize
        then:
            result
    }

    def "should check if correct"() {
        given:
            UserIdChecker userId = new UserId("76082756414")
        when:
            def result = userId.correct
        then:
            result
    }

    def "should check sex"() {
        given:
            UserIdChecker userId = new UserId("76082756414")
        when:
            Optional<UserIdChecker.Sex> result = userId.sex
        then:
            result.get() == UserIdChecker.Sex.MAN
    }

    def "should check date"() {
        given:
            UserIdChecker userId = new UserId("76082756414")
        when:
            Optional<String> result = userId.date
        then:
            result.get() == "27-08-1976"
    }
}
