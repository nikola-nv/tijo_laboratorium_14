package pl.edu.pwsztar;

import java.util.Optional;

final class UserId implements UserIdChecker {

    private final String id;    // NR. PESEL

    public UserId(final String id) {
        this.id = id;
    }

    @Override
    public boolean isCorrectSize() {
        return id.length() == 11;
    }

    @Override
    public Optional<Sex> getSex() {
        int number = Integer.parseInt(String.valueOf(id.charAt(9)));
        Sex sex = number%2==0 ? Sex.WOMAN : Sex.MAN;

        return Optional.of(sex);
    }

    @Override
    public boolean isCorrect() {
        String pattern = "\\d{11}" ;
        int controlNumber = Integer.parseInt(String.valueOf(id.charAt(10)));
        int[] numbers = {9,7,3,1,9,7,3,1,9,7};
        int summarized = 0;

        if(!id.matches(pattern)){
            return false;
        }

        for(int i = 0 ; i < id.length()-1; i++){
            summarized += Integer.parseInt(String.valueOf(id.charAt(i)))*numbers[i];
        }

        return summarized%10 == controlNumber;
    }

    @Override
    public Optional<String> getDate() {
        String lastYearNumbers = id.substring(0,2);
        String day = id.substring(4,6);
        String month = "";
        String year = "";
        int monthAndYear = Integer.parseInt(id.substring(2,4));

        if(monthAndYear > 81 && monthAndYear < 92){
            int monthNumber = monthAndYear-80;
            month = monthNumber > 10 ? String.valueOf(monthNumber) : "0"+monthNumber;
            year = "18"+lastYearNumbers;
        }else if(monthAndYear > 1 && monthAndYear < 12){
            month = monthAndYear > 10 ? String.valueOf(monthAndYear) : "0"+monthAndYear;
            year = "19"+lastYearNumbers;
        }else if(monthAndYear > 21 && monthAndYear < 32){
            int monthNumber = monthAndYear-20;
            month = monthNumber > 10 ? String.valueOf(monthNumber) : "0"+monthNumber;
            year = "20"+lastYearNumbers;
        }else if(monthAndYear > 41 && monthAndYear < 52){
            int monthNumber = monthAndYear-40;
            month = monthNumber > 10 ? String.valueOf(monthNumber) : "0"+monthNumber;
            year = "21"+lastYearNumbers;
        }else if(monthAndYear > 61 && monthAndYear < 72){
            int monthNumber = monthAndYear-60;
            month = monthNumber > 10 ? String.valueOf(monthNumber) : "0"+monthNumber;
            year = "22"+lastYearNumbers;
        }

        return Optional.of(day+"-"+month+"-"+year);
    }
}
